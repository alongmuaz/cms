@extends('account')

@section('content')

    <body class="account-bg">

    <header class="navbar" role="banner">

        <div class="container">

            <div class="navbar-header">
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-cog"></i>
                </button>

                <a href="../landing/index.htm" class="navbar-brand navbar-brand-img">
                    <img src="img/logo.png" alt="MVP Ready">
                </a>
            </div> <!-- /.navbar-header -->

            <nav class="collapse navbar-collapse" role="navigation">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="../landing/index.htm"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Home</a>
                    </li>
                </ul>
            </nav>

        </div> <!-- /.container -->

    </header>

    <br class="xs-80">

    <div class="account-wrapper">

        <div class="account-body">

            <h3>Welcome back to MVP Ready.</h3>

            <h5>Please sign in to get access.</h5>

            <br>

            <div class="row">

                <div class="col-sm-6">
                    <a href="javascript:;" class="btn btn-twitter btn-block">
                        <i class="fa fa-twitter"></i>
                        &nbsp;&nbsp;Login with Twitter
                    </a>
                </div> <!-- /.col -->

                <div class="col-sm-6">
                    <a href="javascript:;" class="btn btn-facebook btn-block">
                        <i class="fa fa-facebook"></i>
                        &nbsp;&nbsp;Login with Facebook
                    </a>
                </div> <!-- /.col -->

            </div> <!-- /.row -->

            <span class="account-or-social text-muted">OR, SIGN IN BELOW</span>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form account-form" method="POST" action="/auth/login">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="login-username" class="placeholder-hidden">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" tabindex="1">
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <label for="login-password" class="placeholder-hidden">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="2">
                </div> <!-- /.form-group -->

                <div class="form-group clearfix">
                    <div class="pull-left">
                        <label class="checkbox-inline">
                            <input type="checkbox" class="" value="" name="remember" tabindex="3"> <small>Remember me</small>
                        </label>
                    </div>

                    <div class="pull-right">
                        <small><a href="/password/email">Forgot Password?</a></small>
                    </div>
                </div> <!-- /.form-group -->

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="4">Signin &nbsp; <i class="fa fa-play-circle"></i></button>
                </div> <!-- /.form-group -->

            </form>

        </div> <!-- /.account-body -->

        <div class="account-footer">
            <p>
                Don't have an account? &nbsp;
                <a href="auth/register" class="">Create an Account!</a>
            </p>
        </div> <!-- /.account-footer -->

    </div> <!-- /.account-wrapper -->


@endsection