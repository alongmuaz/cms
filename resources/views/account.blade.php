
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <title>Signup &middot; MVP Ready</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Google Font: Open Sans -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="{{asset('bower_components/fontawesome/css/font-awesome.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}">

    @if (Auth::user())
    @if (isset($plugincss) && count($plugincss) > 0)
    @foreach($plugincss as $js)
            <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{asset($js)}}">
    @endforeach
    @endif
    @endif


            <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('css/mvpready-admin.css')}}">
    <!-- <link rel="stylesheet" href="./css/custom.css"> -->


    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="account-bg">

@yield('content')

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS -->
<script src="{{asset('bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('bower_components/slimscroll/jquery.slimscroll.min.js')}}"></script>


<!-- App JS -->
<script src="{{asset('global/js/mvpready-core.js')}}"></script>
<script src="{{asset('global/js/mvpready-helpers.js')}}"></script>
<script src="{{asset('js/mvpready-admin.js')}}"></script>


<!-- Demo JS -->
<script src="{{asset('global/js/mvpready-account.js')}}"></script>



</body>
</html>