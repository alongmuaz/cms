@extends('account')

@section('content')

	<header class="navbar" role="banner">

		<div class="container">

			<div class="navbar-header">
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<i class="fa fa-cog"></i>
				</button>

				<a href="{{url('cms.dev')}}" class="navbar-brand navbar-brand-img">
					<img src="{{asset('img/logo.png')}}" alt="MVP Ready">
				</a>
			</div> <!-- /.navbar-header -->

			<nav class="collapse navbar-collapse" role="navigation">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="../landing/index.htm"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Home</a>
					</li>
				</ul>
			</nav>

		</div> <!-- /.container -->

	</header>

	<br class="xs-80">

	<div class="account-wrapper">

		<div class="account-body">

			<h3>Get Started with a Free Account.</h3>

			<h5>Sign up in 30 seconds. No credit card required.</h5>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			<form class="form account-form" method="POST" action="/auth/register">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<label for="signup-email" class="placeholder-hidden">Name</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="Your Name" tabindex="1">
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label for="signup-fullname" class="placeholder-hidden">Email</label>
					<input type="text" class="form-control" id="email" name="email" placeholder="Your Email" tabindex="2">
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label for="signup-username" class="placeholder-hidden">Password</label>
					<input type="text" class="form-control" id="password" name="password" placeholder="Your Password" tabindex="3">
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label for="login-password" class="placeholder-hidden">Password</label>
					<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" tabindex="4">
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label class="checkbox-inline">
						<input type="checkbox" class="" value="" tabindex="5"> I agree to the <a href="javascript:;" target="_blank">Terms of Service</a> &amp; <a href="javascript:;" target="_blank">Privacy Policy</a>
					</label>
				</div> <!-- /.form-group -->

				<div class="form-group">
					<button type="submit" class="btn btn-secondary btn-block btn-lg" tabindex="6">
						Create My Account &nbsp; <i class="fa fa-play-circle"></i>
					</button>
				</div> <!-- /.form-group -->

			</form>

		</div> <!-- /.account-body -->

		<div class="account-footer">
			<p>
				Already have an account? &nbsp;
				<a href="login" class="">Login to your Account!</a>
			</p>
		</div> <!-- /.account-footer -->

	</div> <!-- /.account-wrapper -->

@endsection