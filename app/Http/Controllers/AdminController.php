<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;

class AdminController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function __construct()
	{
		$this->middleware('auth');

		$data['pluginjs'][] = 'bower_components/flot/excanvas.min.js';
		$data['pluginjs'][] = 'bower_components/flot/jquery.flot.js';
		$data['pluginjs'][] = 'bower_components/flot/jquery.flot.pie.js';
		$data['pluginjs'][] = 'bower_components/flot/jquery.flot.resize.js';
		$data['pluginjs'][] = 'bower_components/flot/jquery.flot.time.js';
		$data['pluginjs'][] = 'bower_components/flot.tooltip/js/jquery.flot.tooltip.js';
		$data['pluginjs'][] = 'global/js/demos/flot/line.js';
		$data['pluginjs'][] = 'global/js/demos/flot/pie.js';
		$data['pluginjs'][] = 'global/js/demos/flot/auto.js';

		View::share($data);

		parent::__construct();
	}


	public function index()
	{
		return view('admin.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
