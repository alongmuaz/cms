<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Http\Middleware;
use App\User;
use Illuminate\Support\Facades\View;

class UserController extends BaseController {

	public function __construct()
	{
		$this->middleware('auth');

		// CSS & JS plugin on page array
		$data['plugincss'][] = 'bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css';
		$data['pluginjs'][] = 'bower_components/datatables/media/js/jquery.dataTables.min.js';
		$data['pluginjs'][] = 'bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js';
		$data['pluginjs'][] = 'bower_components/datatables-helper/js/datatables-helper.js';

		View::share($data);
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function datatables()
	{
		$data = User::all()->toJson();
		return $data;

	}


	public function index()
	{
		$data['user'] = User::all();

		return view('admin.user', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{


	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
