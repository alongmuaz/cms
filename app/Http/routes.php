<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

// Route::resource('admin', 'AdminController');

// Route::group(array('prefix' => '/admin') , function() {
// 	Route::resource('admin/user', 'UserController');
// } );

Route::group(['prefix' => 'admin'], function()
{
    Route::resource('users', 'UserController');

    Route::resource('/','AdminController');
});

// Route::resource('admin','AdminController');

// Route::group(['suffix' => 'user'], function() {

// 	Route::resource('/','UserController');
// });


Route::match(['get', 'post'],'admin/user/datatables','UserController@datatables');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
